// Function to create figure properties of uicontrol generation for guibuilder
// Modified on 22-Aug-2023 by Stéphane Mottelet (4.2.2)
// include color_map figure property
function uistring = guicontrolfigurecreate(a)
str(1) = 'figure_position';
str(2) = 'figure_size';
str(3) = 'auto_resize';
str(4) = 'background';
str(5) = 'figure_name';
str(6) = 'color_map';

for cnt = 1:size(str,1) 
    val_temp = get(a,str(cnt));
    if typeof(val_temp) == 'constant'
        val(cnt) = sci2exp(val_temp);
        if size(val_temp,"*") == 1
            val(cnt) = "["+val(cnt)+"]";
        end
    elseif size(val_temp,2) >1
        val(cnt) = strcat(val_temp,'|');
    else
        val(cnt) = val_temp;
    end
end

// Modified on 17-Feb-2017 by Tan Chin Luh (4.0) 
str(7) = 'dockable';
str(8) = 'infobar_visible';
str(9) = 'toolbar_visible';
str(10) = 'menubar_visible';
str(11) = 'default_axes';
str(12) = 'visible';
val(7) = 'off';
val(8) = 'off';
val(9) = 'off';
val(10) = 'off';
val(11) = 'on';
val(12) = 'off';
//

b = strcat([str,val]',''',''');
c = strsubst(b,'''[','[');
d = strsubst(c,']''',']');
uistring ='f=figure('''+d+''');'
// f=figure('figure_position',[400,50],'figure_size',[640,480],'auto_resize','on','background',[33],'figure_name','Graphic window number %d');


endfunction
